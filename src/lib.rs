//! tbp or ToolBox Packages is a package manager for the installation of RPM
//! packages into shared or isolated toolbox container. It serves as an
//! alternative to both rpm-ostree and flatpak and is a sort of middle ground
//! between those two. This package manager is thus destined to Fedora
//! Silverblue.
//!
//! # Exposing packages
//!
//! tbp also _"exposes"_ RPM packages installed into a container outside of it,
//! allowing their "normal" usage without entering in a container, while
//! restricting its execution inside the container.
//!
//! This is done by:
//! 1. Creating bash scripts corresponding to each package's binary containing
//! `toolbox run [-r release] [-c container] binary $@` in `~/.local/bin`.
//! 2. Linking desktop and icon files to
//! `~/.local/share/{applications, icons/hicolor}` from the physical location on
//! the disk on which podman stores them. To avoid duplication and to keep an
//! up-to-date version.
//!
//! # Updating packages
//!
//! # Isolated packages

pub mod package_manager;
pub mod toolbox;

/*
#[cfg(test)]
mod tests {
	use crate::*;

	#[test]
	fn create_container() {
		podman::Container::create(Some("tbp_test_create"), None).unwrap();
	}
}
*/
