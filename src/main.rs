use tbp::package_manager::Package;

#[macro_use]
extern crate clap;
use clap::App;

fn main() {
	let yaml = load_yaml!("cli.yaml");
	let args = App::from_yaml(yaml).get_matches();

	match args.subcommand() {
		("install", Some(args)) => {
			for p in args.values_of("PACKAGE").unwrap() {
				let pkg = Package::new(p).expect("Unable to get toolbox information");
				pkg.install().expect(&format!("Unable to install {}", p));
				if args.is_present("expose") {
					pkg.expose().expect(&format!("Unable to expose {}", p));
				}
			}
		}
		("expose", Some(args)) => {
			for p in args.values_of("PACKAGE").unwrap() {
				let pkg = Package::new(p).expect("Unable to get toolbox information");
				pkg.expose().expect(&format!("Unable to expose {}", p));
			}
		}
		_ => {}
	}
}
