//! Module representing the tbp package manager.

use crate::toolbox::{ Container, read_output };
use std::env;
use std::fs;
use std::io::Write;
use std::option::Option;
use std::os::unix::fs as unixfs;
use std::path::Path;
use std::process::Command;

type Error = Box<dyn std::error::Error>;


/// Struct representing an RPM package.
#[derive(Debug)]
pub struct Package {
	pub name: String,
	pub version: Option<String>,
	pub release: Option<String>,
	pub repository: Option<String>,
	pub container: Container
}

impl Package {
	pub fn new(name: &str) -> Result<Package, Error> {
		Ok(Package {
			name: String::from(name),
			version: None,
			release: None,
			repository: None,
			container: Container::new("fedora-toolbox-32")?,
		})
	}

	pub fn install(&self) -> Result<(), Error> {
		// Check that a package is not already installed.
		if self.is_installed() {
			return Err(
				Box::new(InstalledError {
					package: self.name.to_string()
				})
			);
		}

		// Create arguments.
		let mut args = vec!["sudo", "dnf", "-y", "install"];

		// If some repo is specified, add it to the argument list.
		if let Some(repo) = &self.repository {
			args.push("--repo");
			args.push(&repo);
		}

		args.push(&self.name);

		// Executing arguments in container.
		self.container.execute(&args)?;

		Ok(())
	}

	fn is_installed(&self) -> bool {
		false
	}

	pub fn expose(&self) -> Result<(), Error> {
		// Get all files from a package.
		let files = self.container.execute(&["rpm", "-ql", &self.name])?;

		// Create a vector of paths leading to the packages' internal files.
		let files: Vec<&str> = files
			.lines()
			.filter(|x| x.contains("/")) // Excluding blank lines.
			.collect();

		// Filter binaries (installed in `/usr/bin`).
		let binaries: Vec<&str> = files.iter()
			.filter(|x| x.starts_with("/usr/bin/")
					&& x.split('/').collect::<Vec<&str>>().len() == 4)
			.map(|x| x.split('/').skip(1).nth(2).unwrap()) // Getting binary name.
			.collect();

		// Create the executable directory.
		fs::create_dir_all(Path::new(&format!("{}/.local/bin", env::var("HOME")?)))?;

		// Iterate through each binary.
		for bin in &binaries {
			// Create the file (and delete its content if it already exists).
			let path = &format!("{}/.local/bin/{}", env::var("HOME")?, bin);
			let mut file = fs::File::create(Path::new(path))?;

			// Write the exposed executable content:
			// ```
			// #!/bin/sh
			// toolbox run -c name binary $@
			// ```
			file.write(format!("#!/bin/sh\ntoolbox run -c {} {} $@", self.container.name, bin).as_bytes())?;

			// Make the executable... executable!
			read_output(Command::new("chmod")
						.args(&["755", path])
						.output()?
			)?;
		}

		// Filter application files:
		//   - `/usr/share/applications` for desktop files.
		//   - `/usr/share/icons/hicolor` for icon files.
		//   - `/usr/share/man` for manpages.
		let datas: Vec<&str> = files.iter()
			.filter(|x| x.starts_with("/usr/share/applications/")
					|| x.starts_with("/usr/share/icons/hicolor/")
					|| x.starts_with("/usr/share/man/"))
			.map(|x| *x) // To avoid having to use double references.
			.collect();

		// Getting paths where containers' data is stored.
		let paths = self.container.get_container_paths()?;

		// Iterating through files that needs to be exposed.
		for data in &datas {
			// Iterating though each path to find which contains targeted data.
			for path in &paths {
				// Appending the container's path to the data's path.
				let src = path.clone() + data;
				let src = Path::new(&src);

				// Does the data exist?
				if src.exists() {
					// Splitting the path into a vector according to "/".
					let mut dest = data.split("/").collect::<Vec<&str>>();

					// Appending the user's home.
					let home = env::var("HOME")?;
					dest[0] = &home;

					// Changing "usr" to ".local".
					dest[1] = ".local";

					// Forming the final path.
					let dest = dest.join("/");
					let dest = Path::new(&dest);

					// Creating all prior directories in case they don't exists.
					fs::create_dir_all(dest.parent().unwrap())?;

					if dest.exists() {
						fs::remove_file(dest)?;
					}

					// Symlinking.
					unixfs::symlink(src, dest)?;

					// Breaking the loop, no need to search in other paths.
					break;
				}
			}
		}

		Ok(())
	}
}

/// Error returned if the package is already installed.
#[derive(Debug)]
pub struct InstalledError {
	package: String
}

impl std::fmt::Display for InstalledError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "Package {} is already installed", self.package)
	}
}

impl std::error::Error for InstalledError {}
