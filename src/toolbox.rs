//! Module for controlling ToolBox containers.

use std::fs;
use std::option::Option;
use std::process::Command;
use json;

type Error = Box<dyn std::error::Error>;


/// Struct representing a Container.
#[allow(dead_code)]
#[derive(Debug)]
pub struct Container {
	id: String,
	pub name: String,
	release: u8,
	repositories: Vec<String>
}

impl Container {
	/// Initialize a Container struct from its id or name.
	///
	/// This function will retreive all the information about the specified
	/// container necessary for the good execution of the program.
	///
	/// Examples:
	/// ```rust
	/// use tbp::toolbox::Container;
	///
	/// Container::create(Some("tbp_fetching-toolbox-container-info"), None).unwrap();
	/// let toolbox = Container::new("tbp_fetching-toolbox-container-info").unwrap();
	/// println!("{:#?}", toolbox);
	/// toolbox.remove(true).unwrap();
	/// ```
	pub fn new(id: &str) -> Result<Container, Error> {
		// Inspecting the Container to extract relevant informations.
		let parsed = inspect(id)?;

		// Extracting the Container's name.
		let name = parsed[0]["Name"].to_string();

		// Extracting the Container's release.

		// This is done by retreaving the ImageName which will be on this format:
		// "registry.fedoraproject.org/f#releasever#/fedora-toolbox:#releasever#"
		let rel = parsed[0]["ImageName"].to_string();

		// This will split the ImageName, to extract "#releasever#".
		let rel: Vec<&str> = rel.split(":").collect();

		// This will convert #releasever# to a number.
		let rel: u8 = rel[1].parse()?;

		// Extracting the repository list.

		// This will execute the `dnf repolist -all` command inside the Container.
		// It might output something like this:
		/*
		repo id          repo name                          status
		fedora           Fedora 32 - x86_64                 enabled
		updates-testing  Fedora 32 - x86_64 - Test Updates  disabled
		 */
		let output = read_output(Command::new("toolbox")
			.args(&["run", "-c", &name, "dnf", "repolist", "--all"])
			.output()?)?;

		let repos = output.lines() // We iterate through each line. 
			.skip(1) // We skip first line.
			.map(|x| {
				// We split the line according to whitespaces.
				let s: Vec<&str> = x.split_whitespace().collect();
				// We return the `repo id`.
				s[0].to_string()
			})
			.collect(); // And we collect!

		Ok(Container {
			id: parsed[0]["Id"].to_string(),
			name: name,
			release: rel,
			repositories: repos
		})
	}

	/// Create a toolbox container.
	///
	/// # Examples
	/// ```rust
	/// use tbp::toolbox::Container;
	///
	/// let toolbox = Container::create(Some("tbp_creating-a-toolbox-container"), None).unwrap();
	/// toolbox.remove(true).unwrap();
	/// ```
	///
	/// # Errors
	/// This function should not fail, if it does, it is a bug, please report it.
	pub fn create(name: Option<&str>, rel: Option<u8>) -> Result<Container, Error> {
		let rel = match rel {
			// If the release is specified, we use it.
			Some(r) => r,
			// Or else, we determine the user's fedora release.
			None => {
				// We read the `/etc/os-release` file.
				let o = fs::read_to_string("/etc/os-release")?;

				// And we get the value of the key VERSION_ID.
				o.lines()
					.filter(|x| x.split("=").nth(0) == Some("VERSION_ID")).nth(0)
					.ok_or(NoVersionIDError {})?.split("=")
					.collect::<Vec<&str>>()[1].parse()?
			}
		};

		let name = match name {
			// If a name is specified, we use it.
			Some(n) => String::from(n),
			// Or else, we use the default format.
			None => format!("fedora-toolbox-{}", rel)
		};

		// We create the toolbox.
		read_output(Command::new("toolbox")
			.args(&["create", "-r", &rel.to_string(), "-c", &name])
			.output()?)?;

		// And we return the container with the new name.
		Container::new(&name)
	}

	/// Remove a toolbox container.
	///
	/// # Examples
	/// ```rust
	/// use tbp::toolbox::Container;
	///
	/// let toolbox = Container::create(Some("tbp_removing-a-toolbox-container"), None).unwrap();
	/// toolbox.remove(true).unwrap();
	/// ```
	///
	/// # Errors
	/// This function might fail if the container is running or does not
	/// exists. If the container is running, setting force to true would
	/// solve that, but it'll interrupt all running processes.
	pub fn remove(self, force: bool) -> Result<(), Error> {
		let mut args = vec!["rm"];
		if force {
			args.push("-f");
		}
		args.push(&self.name);
		read_output(Command::new("toolbox")
			.args(&args)
			.output()?)?;
		Ok(())
	}

	/// Execute a command inside a container.
	///
	/// # Examples
	/// You have to provide the arguments in an array as if you would provide them to Command::new()
	/// with the first index being the command name.
	///
	/// ```rust
	/// use tbp::toolbox::Container;
	///
	/// let toolbox = Container::create(Some("tbp_executing-a-toolbox-container"), None).unwrap();
	/// let release = toolbox.execute(&["cat", "/etc/os-release"]).unwrap();
	/// println!("Output:\n{}", release);
	/// toolbox.remove(true).unwrap();
	/// ```
	///
	/// # Errors
	/// This function returns an Error if the container is inaccessible or
	/// the command failed.
	pub fn execute(&self, args: &[&str]) -> Result<String, Error> {
		let mut tb_args: Vec<&str> = vec!["run", "-c", &self.name];
		tb_args.extend(args);
		Ok(read_output(Command::new("toolbox")
			.args(tb_args)
			.output()?)?)
	}

	/// This function returns a vector with all paths where the container's data
	/// is stored.
	pub fn get_container_paths(&self) -> Result<Vec<String>, Error> {
		// Inspecting the container.
		let parsed = inspect(&self.id)?;
		let mut paths: Vec<String> = Vec::new();

		// Getting the first paths (added/modified data).
		let s = parsed[0]["GraphDriver"]["Data"]["UpperDir"].to_string();
		// Paths are separated by a "." in the order importance.
		let s: Vec<&str> = s.split(":").collect();
		// Adding each path to the vector.
		for i in s {
			paths.push(String::from(i));
		}

		// Getting the second paths (original data).
		let s = parsed[0]["GraphDriver"]["Data"]["LowerDir"].to_string();
		let s: Vec<&str> = s.split(":").collect();
		for i in s {
			paths.push(String::from(i));
		}

		Ok(paths)
	}
}

/// Shortcut function to inspect a podman container and then parse the
/// output.
fn inspect(id: &str) -> Result<json::JsonValue, Error> {
	let output = read_output(Command::new("podman")
		.args(&["inspect", id])
		.output()?)?;

	Ok(json::parse(&output)?)
}


/// Error returned in case the release of the OS could not be automatically
/// determined.
#[derive(Debug)]
pub struct NoVersionIDError;

impl std::fmt::Display for NoVersionIDError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "The release could not be automatically determined: no VERSION_ID in /etc/os-release.")
	}
}

impl std::error::Error for NoVersionIDError {}


/// Shortcut function to interpret the output of a command. If the command fails
/// a CommandError is returned.
pub fn read_output(output: std::process::Output) -> Result<String, CommandError> {
	if output.status.success() {
		Ok(String::from_utf8_lossy(&output.stdout).to_string())
	} else {
		Err(CommandError {
			code: output.status.code(),
			stderr: String::from_utf8_lossy(&output.stderr).to_string()
		})
	}
}

/// Error returned if the command fail.
#[derive(Debug)]
pub struct CommandError {
	code: Option<i32>,
	stderr: String
}

impl std::fmt::Display for CommandError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "Command failed, returned {:?}: {}", self.code, self.stderr)
	}
}

impl std::error::Error for CommandError {}
